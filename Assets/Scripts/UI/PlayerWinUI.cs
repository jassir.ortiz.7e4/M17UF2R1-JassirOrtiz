using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerWinUI : MonoBehaviour
{
    public int currentScene = 1; // Index build when the gameplay start
    public int menuScene = 0; // Index build for the menu
    [SerializeField] Sprite cursorTexture;

    public void GoToMenu() {
        SceneManager.LoadScene(menuScene, LoadSceneMode.Single);
        SetDisable();
    }

    public void PlayAgain() {
        SceneManager.LoadScene(currentScene, LoadSceneMode.Single);
        GameManager.Instance.DisableCameraFollowPlayer();
        SetDisable();
    }

    public void SetUp()
    {
        gameObject.SetActive(true);
    }

    private void OnEnable()
    {
        GameManager.Instance.ChangeTextureCursor(cursorTexture);
    }

    public void SetDisable()
    {
        GameManager.Instance.SetDefaultTextureCursor();
        gameObject.SetActive(false);
    }
}
