using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HealtBar : MonoBehaviour
{
    [Header("Control Parameters")]
    [SerializeField] PlayerDataSO playerData;
    public Slider sliderBar;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        
    }

    public void SetMaxHealt() {
        sliderBar.maxValue = playerData.MaxHealt;
        sliderBar.minValue = 0; // On 0 is the minimum value when the player die
    }

    public void SetHealth(float healt) {
        sliderBar.value = healt;
    }
}
