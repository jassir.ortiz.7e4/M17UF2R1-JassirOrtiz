using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class UIGameOver : MonoBehaviour
{
    public int currentScene = 1; // Index build when the gameplay start

    public void OnTextPlayAgain()
    {
        SceneManager.LoadScene(currentScene, LoadSceneMode.Single);
        SetDisable();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            SceneManager.LoadScene(currentScene, LoadSceneMode.Single);
            GameManager.Instance.DisableCameraFollowPlayer();
            SetDisable();
        }
    }

    public void SetUp()
    {
        gameObject.SetActive(true);

    }

    public void SetDisable()
    {
        gameObject.SetActive(false);
    }

}
