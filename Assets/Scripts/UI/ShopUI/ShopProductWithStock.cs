using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


[Serializable]
public class ShopProductWithStock
{
    public ShopItem shopItem;
    public int stock;

    public ShopItem ShopItem
    {
        get { return shopItem; }
    }

    public int Stock
    {
        get { return stock; }
    }
}