using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddBoost : MonoBehaviour, IBought
{
    [SerializeField] private PlayerDataSO playerData;
    public int increase;

    public void OnBought(ShopItem shopItem)
    {
        playerData.speedMovement += increase;
    }
}
