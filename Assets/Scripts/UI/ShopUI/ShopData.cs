using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shop Data", menuName = "Shop Data", order = 54)]
public class ShopData : ScriptableObject
{
    [SerializeField] public List<ShopProductWithStock> items;
}