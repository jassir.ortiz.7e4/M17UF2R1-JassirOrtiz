using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Item Data", menuName = "Item Data", order = 55)]
public class ShopItem : ScriptableObject
{
    public string itemName;
    public int price;
    public Sprite itemSprite;
    public GameObject itemPrefab;
}
