using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShopManager : MonoBehaviour
{
    [SerializeField] PlayerDataSO playerDataSO;
    public ShopProductWithStock[] availableItems;


    public UnityEvent onInsufficienttMoney;
    public UnityEvent onProductNoStock;
    public UnityEvent itemMaxIncrease;

    private void Awake()
    {
        
    }

    public void TryBuyItem(int index) {
        var shopItemWithStock = availableItems[index];
        ShopItem itemToBuy = shopItemWithStock.shopItem;

        if (!playerDataSO.CanAffordThisAmoun(itemToBuy.price))
        {
            onInsufficienttMoney?.Invoke();
            return;
        }

        if (shopItemWithStock.stock == 0)
        {
            onProductNoStock?.Invoke();
            return;
        }

        playerDataSO.RemoveCoins(itemToBuy.price);
        
        GameObject o = Instantiate(itemToBuy.itemPrefab);

        o.GetComponent<IBought>().OnBought(itemToBuy);

        shopItemWithStock.stock = Mathf.Max(shopItemWithStock.stock - 1, 0);

        if (shopItemWithStock.stock == 0)
        {
            onProductNoStock?.Invoke();
        }
    }
}
