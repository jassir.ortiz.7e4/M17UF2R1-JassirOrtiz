using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWeapon : MonoBehaviour, IBought
{
    [SerializeField] private WeaponData weaponData;
    [SerializeField] private PlayerInventory playerInventory;

    public void OnBought(ShopItem shopItem)
    {
        playerInventory.inventory.Add(weaponData);
    }
}

public interface IBought
{
    void OnBought(ShopItem shopItem);
}
