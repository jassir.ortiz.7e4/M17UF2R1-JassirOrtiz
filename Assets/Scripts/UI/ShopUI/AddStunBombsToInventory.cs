using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddStunBombsToInventory : MonoBehaviour
{
    [SerializeField] PlayerDataSO playerData;
    [SerializeField] int price;
    void Start()
    {
        
    }

    public void AddBombsToInventory(int amountToIncrease) {
        playerData.stunBombs += amountToIncrease;

        playerData.RemoveCoins(price);
    }
}
