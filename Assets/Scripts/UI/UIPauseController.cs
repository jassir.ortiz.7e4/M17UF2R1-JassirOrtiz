using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class UIPauseController : MonoBehaviour
{
    [Header("Control Parameters")]
    [SerializeField] Button buttonResume;
    [SerializeField] Button buttonMuteSound;
    [SerializeField] Button buttonGoToMenu;
    [SerializeField] Button buttonQuitGame;
    [SerializeField] AudioListener audioListener;
    [SerializeField] Sprite cursorTexture;
    bool isMute;

    public void ResumeGame() {
        SetDisable();
    }

    public void GoToMenu() {
        SceneManager.LoadScene("MenuScene");
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void SetUp()
    {
        gameObject.SetActive(true);

        
    }

    private void OnEnable()
    {
        Time.timeScale = 0; //This code its for desactivate gameplay
        GameManager.Instance.ChangeTextureCursor(cursorTexture);
    }

    public void SetDisable()
    {
        Time.timeScale = 1; 

        GameManager.Instance.SetDefaultTextureCursor();

        gameObject.SetActive(false);
    }

    public void ManageSound()
   {
       isMute = !isMute;
       AudioListener.volume = isMute ? 0 : 1;

       /*if (isMute)
       {
           buttonMuteSound.image.sprite = muted;
       }
       else
       {
           buttonMuteSound.image.sprite = sound;
       }*/
   }
}
