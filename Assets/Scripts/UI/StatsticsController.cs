using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class StatsticsController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textTotalPoints;
    [SerializeField] TextMeshProUGUI textDeafeatedEnemies;
    [SerializeField] TextMeshProUGUI textCoinsCollected;
    int coinsCollected;
    int totalPoints;
    int defeatedEnemies;

    void Start()
    {
        coinsCollected = PlayerPrefs.GetInt("CoinsCollected");
        totalPoints = PlayerPrefs.GetInt("MaxScore");
        defeatedEnemies = PlayerPrefs.GetInt("EnemiesDeafeated");

        textTotalPoints.text = "TOTAL POINTS: " + totalPoints;
        textDeafeatedEnemies.text = "DEFEATED ENEMIES: " + defeatedEnemies;
        textCoinsCollected.text = coinsCollected.ToString();
    }

}
