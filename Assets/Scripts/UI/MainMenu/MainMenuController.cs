using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [Header("Buttons Parameters")]
    [SerializeField] Button buttonPlay;
    [SerializeField] Button buttonDetails;
    [SerializeField] Button buttonQuit;
    [SerializeField] Button buttonShop;

    [Header("Control Parameters")]
    [SerializeField] Image panelStatstics;

    [Header("Texture Parameters")]
    public Sprite cursorDefaultText;

    void Start()
    {
        Cursor.SetCursor(cursorDefaultText.texture, Vector2.zero, CursorMode.Auto);
    }

    void Update()
    {
        
    }

    private void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panelStatstics.enabled)
            {
                panelStatstics.gameObject.SetActive(false);
            }
        }
    }

    public void SetActivePanelDetailsPlayer() {
        panelStatstics.gameObject.SetActive(true);
    }

    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }



    public void Exit()
    {
        Application.Quit();
    }

    /*public void ManageSound()
   {
       isMute = !isMute;
       AudioListener.volume = isMute ? 0 : 1;

       if (isMute)
       {
           buttonMute.image.sprite = muted;
       }
       else
       {
           buttonMute.image.sprite = sound;
       }
   }*/
}

