using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextController : MonoBehaviour
{
    [Header("Data parameters")]
    [SerializeField] Image controllerWindow;
    DialogDataSO dialogDataSO;

    [Header("Control Parameters")]
    [SerializeField] TextMeshProUGUI textInScreen;
    [SerializeField] float timeToShowDialogue;
    [SerializeField] Sprite cursorTexture;
    public Animator animText;
    public Queue<string> queueDialogue = new Queue<string>();
    public bool playerEndsTutorial;
    float timeToShowControllers = 3;
    bool activateWindowControllers = false;

    private void Start()
    {
        playerEndsTutorial = false;
    }

    public void ActivateWindowsDialogue(DialogDataSO dialogDataSO)
    {
        GameManager.Instance.ChangeTextureCursor(cursorTexture);

        animText.SetBool("DialogueDisapears", false);
        animText.SetBool("DialogueApears", true);
        this.dialogDataSO = dialogDataSO;
    }

    public void ActivateText()
    {

        queueDialogue.Clear();
        foreach (string textToSave in dialogDataSO.dialogToSay)
        {
            queueDialogue.Enqueue(textToSave);
        }

        NextSentence();
    }

    public void NextSentence()
    {
        if (queueDialogue.Count == 0)
        {
            CloseDialogueWindow();
            return;
        }

        string _actualText = queueDialogue.Dequeue();
        //textInScreen.text = _actualText; 
        StartCoroutine(ShowCharacters(_actualText));
    }

    void CloseDialogueWindow()
    {
        animText.SetBool("DialogueApears", false);
        animText.SetBool("DialogueDisapears", true);

        if (!playerEndsTutorial)
        {
            activateWindowControllers = true;
        }
        else
        {
            playerEndsTutorial = true;
        }

        if (GameManager.Instance.playerWins)
        {
            PlayerWon();
        }

        GameManager.Instance.SetDefaultTextureCursor();
    }

    IEnumerator ShowCharacters(string _textToShow)
    {
        textInScreen.text = "";
        foreach (char character in _textToShow.ToCharArray())
        {
            textInScreen.text += character;
            yield return new WaitForSeconds(timeToShowDialogue);
        }
        dialogDataSO = null;
    }

    void PlayerWon()
    {
        GameManager.Instance.PlayerWinsStages();
    }

    private void Update()
    {
        if (activateWindowControllers)
        {
            if (timeToShowControllers > 0)
            {
                timeToShowControllers -= Time.deltaTime;
                PlayerSeeControllers();
            }
            else if (timeToShowControllers < 0)
            {
                controllerWindow.gameObject.SetActive(false);
            }
        }
    }

    void PlayerSeeControllers()
    {
        controllerWindow.gameObject.SetActive(true);
        playerEndsTutorial = true;
    }
}

