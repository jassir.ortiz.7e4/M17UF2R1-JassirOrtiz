using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClickNextDialogue : MonoBehaviour
{
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            FindObjectOfType<TextController>().NextSentence();
        }
    }

    
}
