using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class SpawnerManager : MonoBehaviour
{
    [Header("Enemies to respawn")]
    [SerializeField]
    GameObject[] enemy;

    [Header("Control Parameters")]
    [SerializeField]
    float delayBetweenSpawns = 1f;
    [SerializeField] int maxNumberOfEnemiesToSpawn = 10;
    private GameObject player;
    private bool canSpawn = false;
    int enemiesToEndTutorial = 10;
    int enemiesRespawnedToEndTutorial = 0;

    [Header("Debug")]
    [SerializeField] private List<GameObject> remainingEnemies;
    [SerializeField] private GameObject currentActiveEnemySpawnPoint;
    [SerializeField] float timeRemainingUntilNextSpawn;
    

    private void Awake()
    {
        player = GameObject.Find("Player");
    }


    private void Start()
    {
    }

    private void Update()
    {
        if (delayBetweenSpawns == 0) return;
        timeRemainingUntilNextSpawn = Mathf.Max(timeRemainingUntilNextSpawn - Time.deltaTime, 0);
        if (timeRemainingUntilNextSpawn == 0 && canSpawn)
        {
            SpawnEnemyAtCurrentActiveEnemySpawnPoint();
            if (!GameManager.Instance.playerEndsTutorial) {
                if (enemiesRespawnedToEndTutorial != enemiesToEndTutorial)
                {
                    enemiesRespawnedToEndTutorial += 1;
                }
                else {
                    GameManager.Instance.playerEndsTutorial = true;
                    GameManager.Instance.ActivateWindowsDialogueManager(GameManager.Instance.dialogueEndsTutorial);
                    this.gameObject.SetActive(false);
                }
            }
        }
    }

    public void SpawnEnemyAtCurrentActiveEnemySpawnPoint()
    {
        timeRemainingUntilNextSpawn = delayBetweenSpawns;
        remainingEnemies.RemoveAll(enemy => enemy == null);
        if (remainingEnemies.Count >= maxNumberOfEnemiesToSpawn)
        {
            return;
        }

        GameObject newEnemy = Instantiate(enemy[Random.Range(0, enemy.Length)]);
        newEnemy.transform.position = currentActiveEnemySpawnPoint.transform.position;
        newEnemy.GetComponent<EnemyController>().player = player;
        remainingEnemies.Add(newEnemy);

    }


    public void ChangeEnemySpawnPoint(GameObject newActiveEnemySpawnPoint)
    {
        canSpawn = true;
        currentActiveEnemySpawnPoint = newActiveEnemySpawnPoint;
        SpawnEnemyAtCurrentActiveEnemySpawnPoint();
    }

    public void StopSpawningEnemies()
    {
        canSpawn = false;
    }
}
