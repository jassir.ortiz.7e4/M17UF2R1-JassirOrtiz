using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    [Header("Dialogue Parameters")]
    public DialogDataSO dialogueTutorial;
    public DialogDataSO dialogueEndsTutorial;
    public DialogDataSO dialogueBossFight;
    public DialogDataSO dialoguePlayerWin;

    [Header("Data Parameters")]
    [SerializeField] PlayerDataSO playerData;
    [SerializeField] GameObject player;


    [Header("Control Parameters")]
    [SerializeField] GameObject mainCamera;
    [SerializeField] UIPauseController uiPauseController;
    [SerializeField] UIGameOver uiGameOver;
    [SerializeField] TextController dialogueWindow;
    [SerializeField] PlayerWinUI playerWinUI;
    public bool playerEndsTutorial = false;
    public int enemiesDeafeated;
    public bool playerWins;
    private static GameManager instance;

    [Header("Texture Parameters")]
    public Sprite cursorDefaultText;


    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;

        DontDestroyOnLoad(this.gameObject);
    }


    void Start()
    {
        SetDefaultTextureCursor();

        playerWins = false;

        dialogueWindow.ActivateWindowsDialogue(dialogueTutorial);
    }

    void Update()
    {
        if (playerData.CurrentHealt == 0) {
            PlayerGameOver();
        }
    }

    public void PlayerSetActive() {
        player.SetActive(true);
    }

    public void PlayerSetDisable() {
        player.SetActive(false);
    }

    private void LateUpdate()
    {

    }

    public void PlayerWinsStages() {
        playerWinUI.SetUp();
    }

    public void PlayerGameOver() {
        //Time.timeScale = 0;
        uiGameOver.gameObject.SetActive(true);
        DisableCameraFollowPlayer();
    }

    public void ActivateWindowsDialogueManager(DialogDataSO dialogDataSO) {
        dialogueWindow.ActivateWindowsDialogue(dialogDataSO);
    }

    public void SaveData() {
        PlayerPrefs.SetInt("MaxScore", playerData.totalPoints);
        PlayerPrefs.SetInt("CoinsCollected", playerData.collectedCoins);
        PlayerPrefs.SetInt("EnemiesDeafeated", +enemiesDeafeated);
        PlayerPrefs.SetFloat("PlayerSpeedMovements", playerData.speedMovement);
        PlayerPrefs.Save();
    }
    public void ChangeTextureCursor(Sprite cursorTexture)
    {
        Cursor.SetCursor(cursorTexture.texture, Vector2.zero, CursorMode.Auto);
    }

    public void SetDefaultTextureCursor() {
        Cursor.SetCursor(cursorDefaultText.texture, Vector2.zero, CursorMode.Auto);
    }
    
    public void EnableCameraFollowPlayer() {
        mainCamera.SetActive(true);
    }

    public void DisableCameraFollowPlayer()
    {
        mainCamera.SetActive(true);
    }
}

