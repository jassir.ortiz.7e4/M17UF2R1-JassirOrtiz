using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    [Header("Data Parameters")]
    [SerializeField] EnemyData bossData;

    [Header("Control Parameters")]
    [SerializeField] GameObject range;
    [SerializeField] GameObject Hit;
    public int routine;
    public float chronometer;
    public Animator bossAnimations;
    public int direction;
    public bool isAttacking;
    public float visionRange;
    public float attackRange;
    float offset = 10f;
    GameObject player;
    PlayerAimAssistence playerAimAssistence;


    void Start()
    {
        GameManager.Instance.ActivateWindowsDialogueManager(GameManager.Instance.dialogueBossFight);

        bossAnimations = GetComponent<Animator>();
        playerAimAssistence = FindObjectOfType<PlayerAimAssistence>();
        player = GameObject.Find("Player");

        bossData.ResetParameters();
    }

    void Update()
    {
        Comportamientos();
    }

    public void Comportamientos()
    {
        if (bossData.CurrentHealt <= 0)
        {
            bossAnimations.SetBool("Run", false);
            bossAnimations.SetBool("Attack", false);
            bossAnimations.SetBool("Death", true);
        }
        else
        {
            if (Mathf.Abs(transform.position.x - player.transform.position.x) > attackRange && !isAttacking)
            {
                if (transform.position.x < player.transform.position.x)
                {
                    bossAnimations.SetBool("Run", true);
                    transform.position = Vector2.MoveTowards(transform.position, player.transform.position, bossData.speedMovement * Time.deltaTime);
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    bossAnimations.SetBool("Attack", false);
                }
                else
                {
                    bossAnimations.SetBool("Run", true);
                    transform.position = Vector2.MoveTowards(transform.position, player.transform.position, bossData.speedMovement * Time.deltaTime);
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                    bossAnimations.SetBool("Attack", false);
                }
            }
            else
            {
                if (!isAttacking)
                {
                    if (transform.position.x < player.transform.position.x)
                    {
                        transform.rotation = Quaternion.Euler(0, 0, 0);
                    }
                    else
                    {
                        transform.rotation = Quaternion.Euler(0, 180, 0);
                    }
                    bossAnimations.SetBool("Run", false);
                }
            }

        }
    }

    public void FinalAnimation()
    {
        bossAnimations.SetBool("Attack", false);
        bossAnimations.SetBool("Run", true);
        isAttacking = false;
        range.GetComponent<BoxCollider2D>().enabled = true;
    }

    public void BossDies() {
        bossAnimations.SetBool("Run", false);
        bossAnimations.SetBool("Attack", false);
        bossAnimations.SetBool("Death", false);

        Debug.Log("Boss Death");

        GameManager.Instance.playerWins = true;
        GameManager.Instance.ActivateWindowsDialogueManager(GameManager.Instance.dialoguePlayerWin);
    }

    public void ColliderWeaponTrue()
    {
        Hit.GetComponent<BoxCollider2D>().enabled = true;
    }
    public void ColliderWeaponFalse()
    {
        Hit.GetComponent<BoxCollider2D>().enabled = false;
    }

}
