using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeBoss : MonoBehaviour
{
    public Animator bossAnimation;
    public BossController boss;

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.CompareTag("Player"))
        {
            bossAnimation.SetBool("Run", false);
            bossAnimation.SetBool("Attack", true);
            boss.isAttacking = true;
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
