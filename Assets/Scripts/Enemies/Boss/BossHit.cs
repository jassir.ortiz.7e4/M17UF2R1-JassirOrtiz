using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHit : MonoBehaviour
{
    [Header("Data parameters")]
    [SerializeField] PlayerDataSO playerDataSO;
    [SerializeField] EnemyData bossDataSO;
    PlayerAimAssistence playerAimAssistence;
    PlayerInventory playerInventory;

    void Start()
    {
        playerAimAssistence = FindObjectOfType<PlayerAimAssistence>();
        playerInventory = FindObjectOfType<PlayerInventory>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Ammo")) {
            bossDataSO.CurrentHealt -= playerInventory.inventory[playerAimAssistence.indexWeaponInInventory].Damage;
            Debug.Log(bossDataSO.CurrentHealt);
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            playerDataSO.CurrentHealt -= bossDataSO.damage;
        }
    }    
}
