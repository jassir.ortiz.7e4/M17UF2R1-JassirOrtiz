using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [Header("Scriptable Objetcts")]
    [SerializeField] PlayerDataSO playerData;
    [SerializeField] EnemyData enemyData;

    [Header("Control Parameters")]
    [SerializeField] PlayerInventory playerInventory;
    private PlayerAimAssistence playerAimAssistence;
    public float stunDuration = 2f;
    private float stunTime;
    private bool isStunned = false;
    public GameObject player;
    public Rigidbody2D rb;
    Vector2 visionEnemy;

    [Header("Assistence Parameters")]
    [SerializeField] int possibilityToDropItem;
    [SerializeField] int pointsWhenDie;
    float angleTarget;

    void Start()
    {
        enemyData.ResetParameters();

        playerAimAssistence = FindObjectOfType<PlayerAimAssistence>();
    }

    public void Update()
    {
        if (isStunned) {
            return;
        }
        else {
            EnemyFollowPlayer();
        }
    }

    private void FixedUpdate()
    {
        if (isStunned)
        {
            if (stunTime > 0)
            {
                rb.velocity = Vector2.zero;
                stunTime -= Time.deltaTime;
                return;
            }
            else
            {
                isStunned = false;
            }
        }
    }

    void EnemyFollowPlayer()
    {
        visionEnemy = player.transform.position - transform.position;
        angleTarget = Vector2.Angle(visionEnemy, transform.forward);
        if (angleTarget < 2.5f)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, enemyData.speedMovement * Time.deltaTime);
        }
    }

    public virtual void EnemyDropItem()
    {
        if (Random.Range(0, possibilityToDropItem+1) == possibilityToDropItem) {
            GameObject o = Instantiate(enemyData.ObjectToDrop[Random.Range(0, enemyData.ObjectToDrop.Length)]);
            o.transform.position = transform.position;
        }
    }   

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player")){  
            playerData.CurrentHealt -= enemyData.damage;
            Destroy(this.gameObject);
        }

        if (collision.gameObject.CompareTag("Ammo")) {
            if (enemyData.CurrentHealt > 0)
            {
                enemyData.CurrentHealt -= playerInventory.inventory[playerAimAssistence.indexWeaponInInventory].Damage;
            }
            else {
                playerData.pointsOnLevel += pointsWhenDie;

                EnemyDropItem();

                GameManager.Instance.enemiesDeafeated += 1;

                Destroy(this.gameObject);
            }
        }        
    }

    public void Stun()
    {
        isStunned = true;
        stunTime = stunDuration;
    }

    private void OnDestroy()
    {
        
    }
}
