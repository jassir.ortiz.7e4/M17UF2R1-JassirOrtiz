using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawnOnStart : MonoBehaviour
{
    GameObject player;

    void Start()
    {
        player = GameObject.Find("Player");

        player.transform.position = transform.localPosition;
    }

}
