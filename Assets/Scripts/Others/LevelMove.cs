using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelMove : MonoBehaviour
{
    [Header("Control Parameters")]
    [SerializeField] int sceneBuildIndex;
    public int stageIndexMin;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameManager.Instance.EnableCameraFollowPlayer();

            int randomScene = Random.Range(stageIndexMin, sceneBuildIndex+1);
            SceneManager.LoadScene(randomScene, LoadSceneMode.Single);
        }
    }
}
