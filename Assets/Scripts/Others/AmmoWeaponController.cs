using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoWeaponController : MonoBehaviour
{
    [Header("Control Parameters")]
    [SerializeField] int amountToIncrease;
    PlayerAimAssistence playerAimAssistence;

    private void Start()
    {
        playerAimAssistence = FindObjectOfType<PlayerAimAssistence>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            playerAimAssistence._ammoAmount += amountToIncrease;
            Destroy(this.gameObject);
        }
    }
}
