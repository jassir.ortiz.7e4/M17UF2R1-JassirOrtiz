using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartController : MonoBehaviour
{
    [Header("Control Parameters")]
    [SerializeField] PlayerDataSO playerData;
    [SerializeField] int AmountToIncrease;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (playerData.CurrentHealt < playerData.MaxHealt)
            {
                playerData.CurrentHealt += AmountToIncrease;
                Destroy(this.gameObject);
            }
        }        
    }
}
