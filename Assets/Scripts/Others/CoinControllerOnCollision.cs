using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinControllerOnCollision : MonoBehaviour
{
    [SerializeField] PlayerDataSO playerDataSO;
    [SerializeField] int amountToIncrease;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerDataSO.CollectedCoins += amountToIncrease;
            Destroy(this.gameObject);
        }
    }
}
