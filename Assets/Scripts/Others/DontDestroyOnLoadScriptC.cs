using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoadScriptC : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
