using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropsController : MonoBehaviour
{
    [Header("Info")]
    private Vector3 _startPos;
    private float _timer;
    private Vector3 _randomPos;

    [Header("Settings for shake animation")]
    [Range(0f, 2f)]
    public float _time;
    [Range(0f, 2f)]
    public float _distance;
    [Range(0f, 2f)]
    public float _delayBetweenShakes;

    [Header("Control Parameters")]
    [SerializeField] int hitsToDestroy;
    [SerializeField] GameObject objectToDrop;
    public int probabilityToDropCoin;
    int hits;

    private void Awake()
    {
        _startPos = transform.position;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ammo")) {
            if (hits != hitsToDestroy)
            {
                //animationForProps.SetBool("Animation", true);
                StartCoroutine(Shake());
                hits++;
            }
            else {
                Destroy(this.gameObject);
            }
        }
    }

    private void OnValidate()
    {
        if (_delayBetweenShakes > _time)
            _delayBetweenShakes = _time;
    }

    private IEnumerator Shake()
    {
        _timer = 0f;

        while (_timer < _time)
        {
            _timer += Time.deltaTime;

            _randomPos = (Random.insideUnitSphere * _distance);

            transform.position += _randomPos;

            if (_delayBetweenShakes > 0f)
            {
                yield return new WaitForSeconds(_delayBetweenShakes);
            }
            else
            {
                yield return null;
            }
        }

        transform.position = _startPos;
    }

    void DropCoin() {
        if (Random.Range(0, probabilityToDropCoin+1) == probabilityToDropCoin) {
            GameObject o = Instantiate(objectToDrop) as GameObject;
            o.transform.position = transform.position;
        }
    }

    private void OnDestroy()
    {
        DropCoin();
    }
}
