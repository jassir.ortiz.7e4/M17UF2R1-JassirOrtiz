using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SendTriggerEventOnTagEnter : MonoBehaviour
{
    [SerializeField] private string tagThatTriggersTheEvent = "Player";
    public UnityEvent<GameObject> onTriggerEnter;
    public UnityEvent<GameObject> onTriggerExit;


    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.CompareTag(tagThatTriggersTheEvent))
            return;
        onTriggerEnter?.Invoke(this.gameObject);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.CompareTag(tagThatTriggersTheEvent))
            return;

        onTriggerExit?.Invoke(this.gameObject);
    }
}
