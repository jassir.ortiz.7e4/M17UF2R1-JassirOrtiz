using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DontDestroyOnLoadConditionally : MonoBehaviour
{
    private void Start()
    {
        
    }

    private void Update()
    {
        // Check if the current scene is the one you want the objects to be destroyed in
        if (SceneManager.GetActiveScene().name == "MenuScene")
        {
            Destroy(gameObject);
        }
    }
}
