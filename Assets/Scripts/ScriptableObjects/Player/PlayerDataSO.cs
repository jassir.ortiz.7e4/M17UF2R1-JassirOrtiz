using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New PlayerData", menuName = "New PlayerData", order = 53)]
public class PlayerDataSO : CharacterDataSO
{
    public Object[][] Inventory;
    public int pointsOnLevel;
    public int totalPoints;
    public int collectedCoins;
    public int speedToDash;
    public int stunBombs;

    public int TotalPoints
    {
        get { return totalPoints; }
        set { totalPoints = value; }
    }


    public int StunBombs
    {
        get { return stunBombs; }
        set { stunBombs = value; }
    }

    public int CollectedCoins
    {
        get { return collectedCoins; }
        set { collectedCoins = value; }
    }

    public int PointsOnLevel{
        get { return pointsOnLevel; }
        set { pointsOnLevel = value; }
    }

    public virtual void RemoveCoins(int itemPrice)
    {
        collectedCoins -= itemPrice;
    }

    public virtual bool CanAffordThisAmoun(int itemPrice)
    {
        if (collectedCoins >= itemPrice)
        {
            return true;
        }
        else {
            return false;
        }
    }


    public override void ResetParameters()
    {
        base.ResetParameters();
        pointsOnLevel = 0;
    }
}
