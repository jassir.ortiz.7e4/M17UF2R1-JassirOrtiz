using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New CharacterData", menuName = "New CharacterData", order = 52)]
public class CharacterDataSO : ScriptableObject
{
    [SerializeField] private int _maxHealt;
    [SerializeField] private int _currentHealt;
    public float speedMovement;
   
    public int CurrentHealt {
        get { return _currentHealt; }
        set {
            _currentHealt = value;
        }
    }

    public int MaxHealt
    {
        get { return _maxHealt; }
        set {
            _maxHealt = value;
        }
    }

    public virtual void Awake() { 

    }

    public virtual void ResetParameters()
    {
        _currentHealt = MaxHealt;
    }

    public CharacterDataSO()
    {
        this.Awake();
    }
}
