using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New WeaponData", menuName = "New Weapon", order = 50)]
public class WeaponData : ScriptableObject
{
    public Rigidbody2D _typeAmmo;
    public Sprite _weapon;
    [SerializeField] private float _delay;
    [SerializeField] private int _ammoAmount;
    [SerializeField] private int _damage;
    [SerializeField] private float _speedBullet;
    
    public Sprite Weapon {
        get { return _weapon; }
    }

    public float Delay
    {
        get { return _delay; }
    }

    public float SpeedBullet { 
        get { return _speedBullet; }
    }

    public int AmmoAmount { 
        get { return _ammoAmount; }
        set {
            _ammoAmount = value;
        }
    }

    public int Damage { 
        get { return _damage; }
    }


}
