using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New EnemyData", menuName = "New Enemy", order = 51)]
public class EnemyData : CharacterDataSO
{
    public int damage;
    [SerializeField] private GameObject[] _objectToDrop;
    public GameObject[] ObjectToDrop{
        get { return _objectToDrop;  }
        set {
            _objectToDrop = value;
        }
    }

    public virtual void ResetParameters() {
        CurrentHealt = MaxHealt;
    }
}