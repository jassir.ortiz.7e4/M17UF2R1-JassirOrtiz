using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New DialogData", menuName = "New DialogData", order = 55)]
public class DialogDataSO : ScriptableObject
{
    [TextArea (2,6)]
    public string[] dialogToSay;
    public string[] DialogFromSay { 
        get { return dialogToSay; }
    }
}
