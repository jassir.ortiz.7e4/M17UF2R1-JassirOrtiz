using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunBomb : MonoBehaviour
{
    [SerializeField] Animator animation;
    public float cooldownDuration = 0.75f;
    private float cooldownTime;
    private bool onCooldown = false;

    private void Start()
    {
        cooldownTime = cooldownDuration;
    }

    private void Update()
    {
        if (!onCooldown)
        {
            if (cooldownTime > 0)
            {
                cooldownTime -= Time.deltaTime;
            }
            else
            {
                BombExplosion();
            }
        }
    }

    void BombExplosion() {
        animation.SetBool("BombExplosion", true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy")) {
            Debug.Log("Entro a la colisión");

            collision.gameObject.GetComponent<EnemyController>().Stun();
        }
    }

    public void ColliderWeaponTrue()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }
    public void ColliderWeaponFalse()
    {
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;

        Destroy(this.gameObject);
    }

}
