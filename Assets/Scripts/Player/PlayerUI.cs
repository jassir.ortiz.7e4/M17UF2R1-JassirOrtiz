using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    [Header("Data parameters")]
    [SerializeField] PlayerDataSO playerData;

    [Header("Control Parameters")]
    [SerializeField] PlayerAimAssistence playerAssistence;
    [SerializeField] Image panelPause;
    [SerializeField] Text textAmmoAmount;
    [SerializeField] Text textPointsOnLevel;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            panelPause.gameObject.SetActive(true);
        }
    }


    void LateUpdate()
    {
        textAmmoAmount.text = "Ammo: " + playerAssistence._ammoAmount.ToString();

        textPointsOnLevel.text = "Score: " + playerData.pointsOnLevel.ToString();
    }
}
