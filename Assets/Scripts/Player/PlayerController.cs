using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Control Parameters")]
    [SerializeField] PlayerDataSO playerData;
    [SerializeField] PlayerAnimations playerAnimations;
    [SerializeField] PlayerAimAssistence playerAimAssistence;
    [SerializeField] PlayerPowerUps playerPower;
    public Rigidbody2D rbPlayer;
    public HealtBar healtBar;
    public Vector2 playerMovement;
    int heartIncrease = 5;

    void Awake()
    {
        healtBar.SetMaxHealt();

        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        playerData.ResetParameters();
    }

    void Update()
    {
        if (playerData.CurrentHealt <= 0) {            
            GameManager.Instance.PlayerGameOver();
            Destroy(this.gameObject);
            return;
        }

        // Update input key any moment
        playerMovement.x = Input.GetAxisRaw("Horizontal");
        playerMovement.y = Input.GetAxisRaw("Vertical");        
    }

    private void FixedUpdate()
    {
        // If the player wants do dash
        if (Input.GetKeyDown(KeyCode.LeftShift) && playerPower.canDash)
        {
            playerPower.PlayerDash();
        }

        // If the player wants to throw the stun bomb
        if (Input.GetKeyDown(KeyCode.V) && playerData.stunBombs != 0) {
            playerPower.UseAbilityStunGun();
        }

        Movement();
        ChangeOrientationPlayer();
        
        healtBar.SetHealth(playerData.CurrentHealt);
    }

    private void LateUpdate()
    {
        // Check when the player is running to play the animation
        if (Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Horizontal") < 0 || Input.GetAxisRaw("Vertical") < 0)
        {
            playerAnimations.SetAnimation(true);
        }
        else
        {
            playerAnimations.SetAnimation(false);
        }
    }

    void ChangeOrientationPlayer()
    {
        if (playerAimAssistence.mouse_pos.x < transform.position.x)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
        if (playerAimAssistence.mouse_pos.x > transform.position.x)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }

    public void Movement()
    {
        rbPlayer.MovePosition(rbPlayer.position + playerMovement * playerData.speedMovement * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy")) {
            healtBar.SetHealth(playerData.CurrentHealt);
        }
    }

    private void OnDestroy()
    {
        playerData.totalPoints += playerData.pointsOnLevel;
        GameManager.Instance.SaveData();
    }
}
