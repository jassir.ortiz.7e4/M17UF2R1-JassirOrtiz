using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPowerUps : MonoBehaviour
{
    [Header("Data Parameters")]
    [SerializeField] PlayerDataSO playerData;

    [Header("Control Parameters")]
    [SerializeField] PlayerController playerController;
    public float cooldownDuration = 3f;
    private float cooldownTime;
    private bool onCooldown = false;

    public float dashSpeed = 10f;
    public float dashDuration = 0.5f;
    public float dashCoolDown = 2f;
    public float dashTime;
    public float lastDashTime;
    public bool canDash = true;

    [SerializeField] Vector3 throwVector;
    [SerializeField] GameObject stunBomb;
    [HideInInspector] public Vector3 mouse_pos;
    [SerializeField] Camera cam;
    float speedBomb = 1.25f;
    Vector2 shootVector;
    Vector2 distance;
    Vector3 firePoint;


    public void UseAbilityStunGun()
    {
        if (!onCooldown)
        {
            PlayerThrowBomb();
            onCooldown = true;
            cooldownTime = cooldownDuration;
        }
    }

    public void PlayerThrowBomb()
    {
        Throw();
    }

    void CalculateThrowVector()
    {
        firePoint = new Vector3(transform.position.x, transform.position.y, 1);

        playerData.stunBombs -= 1;

        shootVector = mouse_pos - firePoint;
        shootVector.Normalize();

    }

    void Throw()
    {
        CalculateThrowVector();

        

        GameObject o = Instantiate(stunBomb) as GameObject;
        o.transform.position = transform.position;
        o.GetComponent<Rigidbody2D>().velocity = shootVector * speedBomb;
    }

    void Update()
    {
        if (onCooldown)
        {
            if (cooldownTime > 0 || dashTime > 0)
            {
                cooldownTime -= Time.deltaTime;
            }
            else
            {
                onCooldown = false;
                canDash = true;
            }
        }
    }

    private void LateUpdate()
    {
        mouse_pos = cam.ScreenToWorldPoint(Input.mousePosition);
    }

    private void FixedUpdate()
    {

        if (dashTime > 0)
        {
            playerController.rbPlayer.velocity = transform.right * playerData.speedToDash;
            dashTime -= Time.fixedDeltaTime;
        }
    }

    public void PlayerDash() {
        Dash();
    }

    private void Dash()
    {
        dashTime = dashDuration;
        lastDashTime = Time.time;
        canDash = false;
    }
}
