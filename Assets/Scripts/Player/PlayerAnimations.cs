using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    [Header("Control Parameters ")]
    [SerializeField] Animator animator;
    bool playerIsRunning;


    public void SetAnimation(bool playerIsRunning) {
        this.playerIsRunning = playerIsRunning;

        animator.SetBool("PlayerRun", playerIsRunning);
    }
}
