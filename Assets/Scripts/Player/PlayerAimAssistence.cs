using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimAssistence : MonoBehaviour
{
    [Header("Control Parameters")]
    [SerializeField] Camera cam;
    [SerializeField] PlayerInventory inventoryPlayer;
    [SerializeField] SpriteRenderer weapon;
    Vector3 firePoint;
    Rigidbody2D bullet;
    Rigidbody2D rbBullet;
    float angle;
    float offsetAngleFromSprite = -45f;    

    [Header("Camera Parameters")]
    [HideInInspector] public Vector3 mouse_pos;
    Vector2 shootVector;

    [Header("Asistence Parameters")]
    public int _ammoAmount ;
    [HideInInspector] public int indexWeaponInInventory = 0;
    float cooldown;
    bool shoot = true;

    private void Awake()
    {
        bullet = inventoryPlayer.inventory[indexWeaponInInventory]._typeAmmo;
        weapon.sprite = inventoryPlayer.inventory[indexWeaponInInventory].Weapon;
    }

    void Start()
    {
        rbBullet = GetComponent<Rigidbody2D>();
        weapon = this.gameObject.GetComponent<SpriteRenderer>();

        ChangeWeaponFromInventory();
    }

    private void Update()
    {
        ShootGun();
        CoolDown();

        ChangeWeaponFromInventory();
    }

    private void FixedUpdate()
    {

    }


    private void LateUpdate()
    {
        mouse_pos = cam.ScreenToWorldPoint(Input.mousePosition);
    }

    void ChangeWeaponFromInventory() {
        if (Input.GetKeyDown(KeyCode.X) || Input.GetAxis("Mouse ScrollWheel") > 0f) {

            indexWeaponInInventory++;

            if (indexWeaponInInventory >= inventoryPlayer.inventory.Capacity)
            {
                indexWeaponInInventory = 0;
            }
            if (indexWeaponInInventory < inventoryPlayer.inventory.Capacity)
            {
                bullet = inventoryPlayer.inventory[indexWeaponInInventory]._typeAmmo;
                weapon.sprite = inventoryPlayer.inventory[indexWeaponInInventory].Weapon;
            }
        }
    }

    void ShootGun()
    {
        if (Input.GetMouseButtonDown(0) && shoot && _ammoAmount > 0)
        {
            // Code to play particles when player shoot
            /*if (!particle.isPlaying)
            {
                particle.Play();
                Debug.Log("PARTICULAS EVERYWHERE");
            }

            particle.main.duration.Equals(false);*/

            Shoot();

            cooldown = inventoryPlayer.inventory[indexWeaponInInventory].Delay;
            shoot = false;
        }
    }

    void Shoot()
    {
        _ammoAmount--;

        firePoint = new Vector3(transform.position.x, transform.position.y, 1);

        shootVector = mouse_pos - firePoint;
        shootVector.Normalize();

        angle = Mathf.Atan2(shootVector.y, shootVector.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0, 0, angle + offsetAngleFromSprite); // Weapons sprite has rotation and I fixed with offsetAngle
        rbBullet = Instantiate(bullet, firePoint, Quaternion.identity);
        rbBullet.velocity = shootVector * inventoryPlayer.inventory[indexWeaponInInventory].SpeedBullet; 
    }


    void CoolDown()
    {
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;

        }
        else if (cooldown < 0)
        {
            shoot = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject o = collision.GetComponent<GameObject>();
        if (collision.CompareTag("AmmoIncrease"))
        {
            
            
        }
    }
}
